= LabVIEW project documentation

Tool for automatic documentation of LabVIEW projects.

NOTE: This project is an add-on to https://gitlab.com/wovalab/open-source/labview-doc-generator[Antidoc]

== Goals

Generate documentation of a LabVIEW project.

== Documentation

The documentation is available https://wovalab.gitlab.io/open-source/docs/antidoc-addon-lvproj/latest/index.html[here].

== Building package and dependencies consideration

Before building the package, have a look to https://gitlab.com/wovalab/open-source/antidoc-document-types/labview-project/-/issues/68[this issue] an the related comments 

== Sponsors 

Adding innovative features, improve tools, merge contributor codes, write documentation and support users takes dedication, time and iterations.
With the financial help of the following sponsors, we can focus on delivering better tools to the {lv} developers.

=== Change Makers

https://www.patreon.com/join/wovalab/checkout?rid=9075189[Become a change maker]

=== Innovation Sponsors

https://www.patreon.com/join/wovalab/checkout?rid=9075178[Become an innovation sponsor]

=== Docs Sponsors

https://www.patreon.com/join/wovalab/checkout?rid=9075151[Become a docs sponsor]

=== Merge Request backers

Hampel Software Engineering

https://www.patreon.com/join/wovalab/checkout?rid=9075131[Become a merge request baker]